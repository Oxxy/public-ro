/*
	Queue system
	Made by Plankt
	
	Description of the two functions can be found above
	the function headers below. I tried to comment the
	code well enough to understand every part, if there
	are any doubts, PM me on rathena.org

*/

// The main NPC
-	script	queue_main	-1,{
OnInit:
/*   CONFIGURE THE LINKED EVENTS			

     Add event by assigning the array $@queue<event #>[] with a list of events which it 
     should link to. Example if I want to link event 3 to 1 and 2:
	   setarray $@queue3[0], 1, 2;
     Then players who are in queue for event 1 or 2 will be pulled if you pull for "event 3" 
     If you instead want players from 3 when you pull from event 1 or 2, you can use 2 links:
	   setarray $@queue1[0], 3;
*///   setarray $@queue2[0], 3;

		
//setarray $@queue<n>[0], e1, e2, ... //uncomment and edit this, add as many as you want
		
	/*				 END OF CONFIG					*/
	/*		 DO NOT EDIT ANYTHING BELOW THIS 		*/
	/*		UNLESS YOU KNOW WHAT YOU ARE DOING 		*/
	
// Checks if the table `queue` exists, otherwise it creates it.
	query_sql("CREATE TABLE IF NOT EXISTS `queue` (`id` int(6) NOT NULL AUTO_INCREMENT, `event` int(11) NOT NULL, `name` varchar(30) NOT NULL, `level` smallint(6) NOT NULL, `type` smallint(1) NOT NULL, `aid` int(11) NOT NULL, `size` smallint(4) NOT NULL, `pid` int(11) NOT NULL, `pre` smallint(1) NOT NULL, PRIMARY KEY (`id`))");
end;

// Removes a player or the party from the queue if they log out.
OnPCLogoutEvent:
	set .@id, getcharid(3);
	set .@r, query_sql("SELECT `type`, `pid` FROM `queue` WHERE `aid`='"+.@id+"'", .@type, .@pid);
	if(.@r){
		if(!.@type)
			query_sql("DELETE FROM `queue` WHERE `aid`='"+.@id+"'");
		else
			query_sql("DELETE FROM `queue` WHERE `pid`='"+.@pid+"'");
	}
end;
}

/*
	The push function
	callfunc("push", arg(0), arg(1), arg(2), arg(3));
	0) Type of input (0 for Player, 1 for Party, 2 for Guild)
	1) Event number (0 means all events)
	2) Level required for the event
	3) Should the member get priority? (0 for No (default), 1 for if the player is GM, 2 for Yes)
	Returns: 0 if successful, 1+ if error occured
	
	This is the function used when adding players, parties or guilds to the queue.
*/
function	script	push	{
// Get the input arguments, check above for meaning
	set .@type, getarg(0,0);
	set .@event, getarg(1,0);
	set .@lvlreq, getarg(2,0);
	set .@pre, 0;
	
// Do not continue if a player is not attached
	set .@id, playerattached();
	if(!.@id) return 1;
	
	if(BaseLevel < .@lvlreq) return 2;
	
// Check what kind of type we want to push
	switch(.@type){
		case 0: // Player
			set .@pre, getarg(3,0); // Check priority
			if(.@pre == 1 && !getgmlevel()) set .@pre, 0;
			
			set .@names$, strcharinfo(0);
			set .@level, BaseLevel;
			set .@aid, .@id;
			set .@size, 1;
			set .@party, 0;
		break;
		case 1: // Party
			set .@party, getcharid(1);
			if(!.@party){
				return 3;
			}
			getpartymember .@party,2;
			set .@size, $@partymembercount;
			if(.@size < 2){
				return 4;
			}
			copyarray .@aid[0], $@partymemberaid[0], .@size;
		// For every partymember, check online, name and blvl
			for(set .@i, 0; .@i < .@size; set .@i, .@i+1){
				if(!attachrid(.@aid[.@i])){
					attachrid .@id;
					return 5;
				}
				if(BaseLevel < .@lvlreq){
					attachrid .@id;
					return 6;
				}
				set .@names$[.@i], strcharinfo(0);
				if(.@level < BaseLevel) set .@level, BaseLevel;
			}
			attachrid .@id;
		break;
		case 2: // Guild
			set .@aid, getcharid(2);
			if(!.@aid){
				return 8;
			}
			if(getguildmasterid(.@aid) != getcharid(0)){
				return 9;
			}
			set .@names$, getguildname(.@aid);
			set .@level, 0;
			set .@size, 0;
			set .@party, 0;
		break;
	}
	
	// Check if any of the players already is in the queue
	set .@query$, "SELECT 1 FROM `queue` WHERE `aid`='"+.@aid+"'";
	for(set .@i, 1; .@i < .@size; set .@i, .@i+1){
		set .@query$, .@query$+" || `aid`='"+.@aid[.@i]+"'";
	}
	if(query_sql(.@query$,.@dummy)){
		return 7;
	}
	
	// Puts the query together to add all players at the same time
	// to make sure they aren't spread out from eachother
	set .@query$, "INSERT INTO `queue` (`event`,`name`,`level`,`type`,`aid`,`size`,`pid`,`pre`) VALUES ('"+.@event+"', '"+.@names$+"', '"+.@level+"', '"+.@type+"', '"+.@aid+"', '"+.@size+"', '"+.@party+"', '"+.@pre+"')";
	for(set .@i, 1; .@i < .@size; set .@i, .@i+1){
		set .@query$, .@query$+",('"+.@event+"', '"+.@names$[.@i]+"', '"+.@level+"', '"+.@type+"', '"+.@aid[.@i]+"', '"+.@size+"', '"+.@party+"', '"+.@pre+"')";
	}
	query_sql(.@query$);
	return 0;
}

/*
	The pull function
	callfunc("pull", arg(0), arg(1), arg(2), arg(3), arg(4));
	0) Type of output (0 for Any, 1 for Players only, 2 for Parties only, 3 for Guilds only)
	1) Event number (0 means all events, not supported yet)
	2) Number of players wanted
	3) Strict quota (In the chance that there aren't enough players in the queue, 0 returns those found, 1 returns error)
	   If set to 1 with Type set to 2, it will search for a party of that size
	Returns: 0 if successful, 1+ if error occured
	
	This is the function used when getting players, parties or guilds from the queue.
	It sets 4 temporary global variables, make sure to save them before they are overwritten.
	
	$@queue_size	The amount of players returned
	$@queue			The Account IDs of players retrieved
*/
function	script	pull	{
// Get the input arguments, check above for meaning
	set .@type, getarg(0,0);
	set .@event, getarg(1,0);
	set .@size, getarg(2,0);
	if(!.@size) return 1;
	set .@fill, getarg(3,0);
	
// Build the query to collect players wanted
	set .@query$, "SELECT `aid`,`size` FROM `queue` WHERE (`event`='0'";
// Checks if the event is linked
	set .@ls, getarraysize(getd("$@queue"+.@event));
	if(.@ls){
		for(set .@i, 0; .@i < .@ls; set .@i, .@i+1){
			set .@query$, .@query$+" || `event`='"+getd("$@queue"+.@event+"["+.@i+"]")+"'";
		}
	} else {
		set .@query$, .@query$+" || `event`='"+.@event+"'";
	}
	set .@query$, .@query$+")";
	
	switch(.@type){
		case 1: //Players only
			set .@query$, .@query$+" && `type`='0'";
		break;
		case 2: //Parties only
			set .@query$, .@query$+" && `type`='1'";
		break;
		case 3: //Guilds only
			set .@query$, .@query$+" && `type`='2'";
		break;
		default:
			set .@query$, .@query$+" && `type`!='2'";
		break;
	}
	
// Makes sure the table is only accessed by one instance at a time
// If the table is in use, it will pause until it's availiable
	while($@pull_lock) sleep2 10;
	set $@pull_lock, 1;
	sleep2 1; // If you have problems setting the global vars in time, increase this.
	// Gets the availiable queue
		set .@rn, query_sql(.@query$+" && `pre`='0' ORDER BY `id` ASC LIMIT 127", .@aid, .@psize);
	// Checks if it's a guild
		if(.@type == 3){
			if(!.@rn){
				set $@pull_lock, 0;
				return 2;
			}
			set $@queue, .@aid;
			set $@queue_size, 1;
		} else {
		// Checks for prioritized players
			if(.@type < 2){
				set .@pn, query_sql(.@query$+" && `pre`>'0' ORDER BY `id` ASC LIMIT 127", .@aid2, .@psize2);
				if(.@pn){
					copyarray .@aid2[.@pn], .@aid[0], .@rn;
					copyarray .@aid[0], .@aid2[0], getarraysize(.@aid2);
					copyarray .@psize2[.@pn], .@psize[0], .@rn;
					copyarray .@psize[0], .@psize2[0], getarraysize(.@psize2);
					set .@rn, .@rn+.@pn;
				}
			}
		// Check if we got the required amount of players
			if(.@rn < .@size && .@fill || !.@rn){
				set $@pull_lock, 0;
				return 2;
			}
		// Fills the places up with players and parties according to the passed arguments
			set .@j, 0;
			for(set .@i, 0; .@size && (.@i < .@rn); set .@i, .@i+1){
				if(.@psize[.@i] == 1){ // Player
					set $@queue[.@j], .@aid[.@i];
					set .@size, .@size-1;
					set .@j, .@j+1;
				} else { // Party
					if(.@size >= .@psize[.@i] && (!.@fill || .@type != 2) || .@fill && .@size == .@psize[.@i]){
						copyarray $@queue[.@j], .@aid[.@i], .@psize[.@i];
						set .@size, .@size-.@psize[.@i];
						set .@j, .@j+.@psize[.@i];
					}
					set .@i, .@i+.@psize[.@i]-1;
				}
			}
			set $@queue_size, .@j;
			if(.@size && .@fill || !.@j){
				set $@pull_lock, 0;
				return 3;
			}
		}
		
	// Remove the found players from the queue
		set .@query$, "DELETE FROM `queue` WHERE `aid`='"+$@queue+"'";
		for(set .@i, 1; .@i < $@queue_size; set .@i, .@i+1){
			set .@query$, .@query$+" || `aid`='"+$@queue[.@i]+"'";
		}
		query_sql(.@query$);
		
	//Team support to be added
		
	set $@pull_lock, 0;
	return 0;
}